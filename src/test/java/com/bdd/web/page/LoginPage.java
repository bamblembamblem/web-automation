package com.bdd.web.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//POM _ String
//POM_ By.
//POM + PageFACTORY_@FindBy
public class LoginPage {

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "user-name")
    private WebElement userInput;
    @FindBy(id = "password")
    private WebElement passInput;
    @FindBy(id = "login-button")
    private WebElement loginButton;

    @FindBy(xpath = "//h3[@data-test='error']")
    private WebElement messageError;

    //ACTIONS
    public void typeUser(String user) {
        userInput.sendKeys(user);
    }

    public void typePass(String pass) {
        passInput.sendKeys(pass);
    }

    public void login(){
        loginButton.click();
    }

    public String getError(){
        return messageError.getText();
    }

}
