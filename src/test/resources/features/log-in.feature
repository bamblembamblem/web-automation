#language: es
@LOGIN_REG
Característica: Log-in en SauceDemo
  Yo como usuario de SauceDemo
  Quiero ingresar a la plataforma
  Para buscar productos

  @HAPPY_PATH @smokeTest @end2end
  Escenario: Login con las credenciales del usuario correctas
    Dado que estoy en la plataforma de SauceDemo
    Cuando ingreso las credenciales del usuario
      | user          | pass         |
      | standard_user | secret_sauce |
    Entonces se debería validar que estoy en el home de la plataforma

  @FAIL @smokeTest
  Escenario: Login incorrecto con el password vacio
    Dado que estoy en la plataforma de SauceDemo
    Cuando ingreso las credenciales del usuario
      | user          | pass |
      | standard_user |      |
    Entonces se debería validar que se muestra el mensaje de error "Epic sadface: Password is required"

  @FAIL
  Escenario: Login incorrecto con el password incorrecto
    Dado que estoy en la plataforma de SauceDemo
    Cuando ingreso las credenciales del usuario
      | user          | pass      |
      | standard_user | asdasdasd |
    Entonces se debería validar que se muestra el mensaje de error "Epic sadface: Username and password do not match any user in this service"